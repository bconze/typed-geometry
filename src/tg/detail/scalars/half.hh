#pragma once

namespace tg
{
// software emulation of 16bit math
struct half
{
    constexpr half() = default;
    constexpr half(float f)
    {
        // TODO
        data = 0;
    }

    constexpr operator float() const
    {
        // TODO
        return 0;
    }

    // TODO
private:
    unsigned short data = 0;
};

static_assert(sizeof(half) == 2, "half is not 16bit");

// TODO
constexpr half operator-(half const& v) { return v; }
constexpr bool operator<(half const& a, float b) { return true; }
constexpr bool operator<(half const& a, int b) { return true; }
} // namespace tg
