#pragma once

#include "operators/common.hh"

#include "operators/ops_mat.hh"
#include "operators/ops_pos.hh"
#include "operators/ops_size.hh"
#include "operators/ops_triangle.hh"
#include "operators/ops_vec.hh"
