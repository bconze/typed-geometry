#pragma once

#include "scalar.hh"

#include "color.hh"
#include "pos.hh"
#include "size.hh"
#include "vec.hh"

#include "quadric.hh"

#include "quat.hh"

#include "mat.hh"
#include "transform.hh"

#include "objects/objects.hh"
