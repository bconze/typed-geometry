#pragma once

// Lean header that only includes type definitions

#include "types/types.hh"

// special values for static values such as vec3::zero
#include "detail/special_values.hh"
