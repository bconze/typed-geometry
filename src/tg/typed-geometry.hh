#pragma once

#include "typed-geometry-lean.hh"

#include "detail/constants.hh"
#include "detail/limits.hh"
#include "detail/random.hh"
#include "detail/scalar_traits.hh"
#include "detail/special_values.hh"
#include "detail/tg_traits.hh"
#include "detail/utility.hh"

#include "detail/scalars/half.hh"
#include "detail/scalars/quarter.hh"
#include "detail/scalars/scalar_math.hh"

#include "detail/functions.hh"
#include "detail/operators.hh"
